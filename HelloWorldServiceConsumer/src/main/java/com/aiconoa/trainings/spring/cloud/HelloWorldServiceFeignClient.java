package com.aiconoa.trainings.spring.cloud;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("helloworldservice")
public interface HelloWorldServiceFeignClient {

	@RequestMapping(method = RequestMethod.GET, value="/hello")
	String getMessage();
	
}
