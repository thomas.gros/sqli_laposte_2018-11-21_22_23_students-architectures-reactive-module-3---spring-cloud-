package com.aiconoa.trainings.spring.cloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class HelloWorldConsumerRestController {
	private static final Logger LOGGER = 
			LoggerFactory.getLogger(HelloWorldConsumerRestController.class);

	
	// définition du bean
	@Bean
	@LoadBalanced
	private WebClient.Builder webClientBuilder() {
		return WebClient.builder();
	}
	
	// injection du bean
	@Autowired
	private WebClient.Builder webClientBuilder;
	
	@GetMapping("/consume-hello")
	public Mono<String> consumeHello() {
		LOGGER.info("request /consume-hello");
		return
//			WebClient
//			.create("http://localhost:8080")
//			 webClientBuilder.baseUrl("http://localhost:8082").build()
			webClientBuilder.baseUrl("http://helloworldservice").build()
			.get()
			.uri("/hello")
			.retrieve()
			.bodyToMono(String.class)
			.map(m -> String.format("consumed %s from HelloWorldRestService ;)", m));
	}
	
	@GetMapping(path="/consume-hello-multi", produces=MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<String> consumeHelloSSE() {
		return
			webClientBuilder.baseUrl("http://helloworldservice").build()
			.get()
			.uri("/hello-multi")
			.exchange()
			.flatMapMany(cr -> cr.bodyToFlux(String.class)); // https://stackoverflow.com/questions/48254774/what-is-then-thenempty-thenmany-and-flatmapmany-in-spring-webflux
	}
	
	@Autowired
	private HelloWorldServiceFeignClient feignClient;
	
	@GetMapping("/consume-with-feign")
	public String consumeViaFeign() {
		return feignClient.getMessage();
	}
	
}
