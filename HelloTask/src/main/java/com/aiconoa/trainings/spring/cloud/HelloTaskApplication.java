package com.aiconoa.trainings.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloTaskApplication.class, args);
	}
}
