package com.aiconoa.trainings.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableBinding(Sink.class)
public class TaskEventSink {

	public static void main(String[] args) {
		SpringApplication.run(TaskEventSink.class, args);
	}
	
	@StreamListener(Sink.INPUT)
	public void taskEvent(String message) {
		System.out.println(message);
	}
}