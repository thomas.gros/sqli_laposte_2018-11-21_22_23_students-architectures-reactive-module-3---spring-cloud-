package com.aiconoa.trainings.spring.cloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Transformer;

@SpringBootApplication
@EnableBinding(Processor.class)
public class CloudStreamTransformerApplication {

	private static Logger LOGGER 
		= LoggerFactory.getLogger(CloudStreamTransformerApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(CloudStreamTransformerApplication.class, args);
	}
	
	public static class Data {
		private String value;
		
		public String getValue() {
			return value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Data [value=" + value + "]";
		}

	}
	
	@Transformer(inputChannel=Processor.INPUT,
			     outputChannel=Processor.OUTPUT)
	public Data transform(String message) {
		LOGGER.info("transformer received " + message);
		Data d = new Data();
		d.setValue(message);
		return d;
	}
	
}
