package com.aiconoa.trainings.spring.cloud;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class HelloWorldRestController {
	private static final Logger LOGGER = 
			LoggerFactory.getLogger(HelloWorldRestController.class);

	
	@Value("${msg:default}")
	private String messageFromConfigServer;
	
	@GetMapping("/hello")
	public Mono<String> sayHello() {
		LOGGER.info("request /hello");
		return Mono.just("hello world " + messageFromConfigServer);
	}
	
	@GetMapping(path="/hello-multi", produces=MediaType.TEXT_EVENT_STREAM_VALUE) // ServerSentEvent
	public Flux<String> sayHelloMultipleTimes() {
		
		return Flux
				.interval(Duration.ofMillis(100))
				.map(t -> "hello " + t);
	}
	
	@GetMapping(path="/hello-multi-sse-full", produces=MediaType.TEXT_EVENT_STREAM_VALUE) // ServerSentEvent
	public Flux<ServerSentEvent<String>> sayHelloMultipleTimesSSEFull() {
		
		return Flux
				.interval(Duration.ofMillis(100))
				.map(t ->
					ServerSentEvent
						.<String>builder()
						.id(""+t)
						.data(""+t)
						.event("hello")
						.comment("un comment")
						.build()
				);
	}
	
	
}
