package com.aiconoa.trainings.spring.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.support.GenericMessage;

@SpringBootApplication
// @EnableBinding(Source.class)
@EnableBinding(value= {Source.class, MyBindings.class})
public class CloudStreamSourceApplication {
	// on envoie des messages en polling toutes les X secondes
	@Bean
	@InboundChannelAdapter(
			value = Source.OUTPUT,
			poller= @Poller(fixedDelay="500", maxMessagesPerPoll="1")
			)
	public MessageSource<String> sendMessagePollingSource() {
		return () -> new GenericMessage<>("hello");
	}
	
	// on récupère l'implémentation du bindinds
	// cela nous donnera accès direct aux Channel Source pour émettre
	@Autowired
	private Source source;
	
	// on envoie des messages avec plus de contrôle
	@Bean
	public CommandLineRunner cli() {
		return args -> {
			for(int i = 0; i < 1000; i++) {
				source.output().send(
					MessageBuilder.withPayload("from CLI").build());
			}
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(CloudStreamSourceApplication.class, args);
	}
}
