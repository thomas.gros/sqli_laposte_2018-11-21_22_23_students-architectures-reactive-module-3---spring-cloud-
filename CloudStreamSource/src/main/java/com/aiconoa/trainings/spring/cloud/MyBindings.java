package com.aiconoa.trainings.spring.cloud;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface MyBindings {

	@Input("topic-toto")
	SubscribableChannel myInput1();
	
	@Input
	SubscribableChannel myInput2();
	
	@Output
	MessageChannel myOutput1();
	
	@Output
	MessageChannel myOutput2();
	
}
