package com.aiconoa.trainings.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@SpringBootApplication
//@EnableCircuitBreaker
//@EnableDiscoveryClient
@SpringCloudApplication
public class HelloWorldServiceConsumerHystrixApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldServiceConsumerHystrixApplication.class, args);
	}
}
