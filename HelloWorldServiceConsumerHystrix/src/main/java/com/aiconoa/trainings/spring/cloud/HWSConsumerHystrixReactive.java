package com.aiconoa.trainings.spring.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.HystrixCommands;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@RestController
public class HWSConsumerHystrixReactive {

	@Bean
	@LoadBalanced
	public WebClient.Builder clientBuilder() {
		return WebClient.builder();
	}

	@Autowired
	private WebClient.Builder webClientBuilder;
	
	@GetMapping("/consume-hystrix-reactive")
	public Mono<String> consumeHelloWorldService() {
			return HystrixCommands
						.from(helloCommand())
						.fallback(fallCommand())
						.commandName("hello-command")
						.toMono();				
	}
	
	public Mono<String> helloCommand() {
		return webClientBuilder
				.baseUrl("http://helloworldservice")
				.build()
				.get()
				.uri("/hello")
				.retrieve()
				.bodyToMono(String.class);
	}
	
	public Mono<String> fallCommand() {
		return Mono.just("fallback");
	}
	
	
	
	
	
	
	
	
	
}
