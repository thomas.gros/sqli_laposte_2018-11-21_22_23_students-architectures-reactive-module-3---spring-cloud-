package com.aiconoa.trainings.spring.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HWSConsumerHystrixNonReactive {

	@Autowired
	private HelloWorldService helloWorldService;
	
	@GetMapping("/consume-hystrix")
	public String consumeViaHystrix() {
		return helloWorldService.helloCommand();
	}
	
}
