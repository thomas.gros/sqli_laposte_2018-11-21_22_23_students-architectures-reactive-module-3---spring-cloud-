package com.aiconoa.trainings.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}

	@Bean
	public RouteLocator routes(RouteLocatorBuilder builder) {
			return builder
					.routes()
					.route("get-httpbin", r -> r.path("/get")
												.uri("http://httpbin.org"))
					.route("helloworldservice-hello", 
							r -> r.path("/hello")
								  .filters(f -> 
								  f.addRequestHeader("hello", "world")
								   .addResponseHeader("hello-rep", "world-rep"))
								   .uri("lb://helloworldservice"))
					.build();
		
	}

}
